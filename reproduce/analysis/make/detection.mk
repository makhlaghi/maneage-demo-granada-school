# Project initialization.
#
# Copyright (C) 2018-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.



# Directory to hold detection inputs.
detdir=$(badir)/detection
$(detdir): | $(badir); mkdir $@



# Run the detection with NoiseChisel.
det = $(detdir)/nc.fits
$(det): $(indir)/HCG16_CD_rob2_MS.pbcor.fits \
        $(pconfdir)/noisechisel.conf | $(detdir)
	astnoisechisel $< -h0 --output=$@ --label \
	               --config=$(pconfdir)/noisechisel.conf \
	               --config=$(installdir)/etc/astnoisechisel-3d.conf





# Generate a catalog of the detections
detcat = $(detdir)/cat.fits
$(detcat): $(det)
	astmkcatalog $< -hDETECTIONS --output=$@ --ids \
	             --min-x --max-x --min-y --max-y --min-z --max-z \
	             --sum





# A 2D image of the main detection.
figdir=$(texdir)/figures
det2d = $(figdir)/det2d.pdf
$(figdir):; mkdir $@
$(det2d): $(detcat) | $(figdir)

#	Delete any existing TiKZ picture.
	rm -f $(tikzdir)/det2d.*

#	Mask all the pixels that don't belong to this object.
	masked=$(subst .pdf,-masked.fits,$@)
	astarithmetic $(det) -hINPUT-NO-SKY set-i \
	              $(det) -hDETECTIONS   set-d \
	              i d 1 ne nan where -o$$masked

#	Crop the desired region.
	crop=$(subst .pdf,-crop.fits,$@)
	eval "$$(asttable $(detcat) --equal=OBJ_ID,1 \
	                 -cMIN_X,MAX_X,MIN_Y,MAX_Y,MIN_Z,MAX_Z \
                         | xargs printf "sec=%d:%d,%d:%d,%d:%d")"
	astcrop $$masked --mode=img --section=$$sec --output=$$crop

#	Make the 2D demo image and convert it to PDF.
	crop2d=$(subst .pdf,-crop-2d.fits,$@)
	astarithmetic $$crop 3 collapse-sum -o$$crop2d
	astconvertt $$crop2d --fluxhigh=0.02 --colormap=sls -o$@

#	Delete the temporary files.
	rm $$masked $$crop $$crop2d





# Final LaTeX macros.
$(mtexdir)/detection.tex: $(det2d) | $(mtexdir)

#	Write the NoiseChisel configuration parameters.
	echo "\newcommand{\nckernel}{$(kernel)}" > $@
	echo "\newcommand{\ncqthresh}{$(qthresh)}" >> $@
	echo "\newcommand{\ncerode}{$(erode)}" >> $@
	echo "\newcommand{\ncnoerodequant}{$(noerodequant)}" >> $@
	echo "\newcommand{\ncdetgrowquant}{$(detgrowquant)}" >> $@

#	Meaurements
	u=$$(astfits $(det) -hINPUT-NO-SKY --keyvalue=BUNIT -q)
	echo "\newcommand{\cubeunit}{$$u}" >> $@
	s=$$(asttable $(detcat) --equal=OBJ_ID,1 -cSUM -Y)
	echo "\newcommand{\totalflux}{$$s}" >> $@
